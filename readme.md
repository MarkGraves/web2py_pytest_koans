# Learn web2py through using pytest

## Goals:
1. Help a new developer learn to use python through tdd
2. Help a new developer get up to speed with web app development in the web2py framework
    1. Learn the general pattern of Request, Model, Controller, View, Response
    2. Learn about web2py "magic" to accelerate process of web development
3. As a new developer/entrepreneur, it is important to ensure maximum viable integrity
    1. Be able to always make sure code that you are pushing is not going to break the server

### Repo includes a distro of web2py for simplification, just go into directory and type py.test

### To skip web2py tests, cd to directory maximum_viable_integrity, then run py.test

