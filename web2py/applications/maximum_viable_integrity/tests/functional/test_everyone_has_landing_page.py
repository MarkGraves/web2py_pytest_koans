# Every user should have a landing page with a successful build of the app
import pytest

from pytest_bdd import scenario
from pytest_bdd import given
from pytest_bdd import when
from pytest_bdd import then

@scenario('../features/everyone_has_a_landing_page.feature','Everyone should see a landing page')
def test_everyone_has_a_landing_page(client):
    pass

@given('I am visiting my web site')
def I_am_visiting_my_web_site():
    pass


@when('I enter the base url in my browser')
def I_enter_the_base_url_in_my_browser(client):
    base_url = '/default/index'

    client.get(base_url)
    return client

@then('The landing page should successfully render')
def The_landing_page_should_successfully_render(web2py,client):
    # landing page should render response 200 OK
    assert(client.status == '200')
    assert('Maximum Viable Integrity' in client.text)
    result = web2py.run('default', 'index', web2py)
    html = web2py.response.render('default/index.html')
    assert ('Maximum Viable Integrity' in html)

    pass
