# Every user should have a landing page with a successful build of the app
import pytest

from pytest_bdd import scenario
from pytest_bdd import given
from pytest_bdd import when
from pytest_bdd import then

@scenario('../features/user_can_register.feature','Unregistered users should be able to register')
def test_user_can_register(client):
    pass

@given('I am not logged in')
def I_am_not_logged_in(web2py):
    assert(web2py.auth.user_id is None)

@when('I go to the registration page')
def I_go_to_the_registration_page(web2py,client):
    registration_url = '/default/user/register'

    client.get(registration_url)
    return client

@when('I input my registration data')
def I_input_my_registration_data(web2py,client):
    data = dict(first_name='Homer',
                last_name='Simpson',
                email = 'user@domain.com',
                password = 'password',
                password_two = 'password')
    client.post('/default/user/register', data=data)

    return client



@then('The application should register my data')
def The_application_should_register_my_data(web2py,client):
    assert client.status == 200
    data = dict(first_name='Homer',
                last_name='Simpson',
                email = 'user@domain.com',
                password = 'password',
                password_two = 'password')
    assert web2py.db(web2py.db.auth_user).count() == 1
    assert web2py.db(web2py.db.auth_user == data['name']).count() == 1

    pass
