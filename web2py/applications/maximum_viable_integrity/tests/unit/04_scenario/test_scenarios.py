from pytest_bdd import scenario
from pytest_bdd import given
from pytest_bdd import when
from pytest_bdd import then

import pytest

@given('I have a given step')
def given_statement():
    return 'given'

@when('I have a when step')
def when_statement(given_statement):
    assert ('I have a given step' == 'I have a %s step' % given_statement)
    return 'when'

@then('I have given and when steps')
def then_statement(given_statement,when_statement):
    assert ('I have given and when steps' == 'I have %s and %s steps' % (given_statement,when_statement))

def test_scenario_without_steps(request):
    @scenario(feature_name='scenarios_syntax.feature',scenario_name='I have a scenario without any steps')
    def test():
        pass

    test(request)

def test_scenario_with_given_step(request):
    @scenario(feature_name='scenarios_syntax.feature',scenario_name='I have a scenario with a given step')
    def test():
        pass

    test(request)

def test_scenario_with_given_and_when_steps(request):
    @scenario(feature_name='scenarios_syntax.feature',scenario_name='I have a scenario with given and when steps')
    def test():
        pass

    test(request)
