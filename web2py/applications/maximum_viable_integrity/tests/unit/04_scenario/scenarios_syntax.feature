Scenario: I have a scenario without any steps

Scenario: I have a scenario with a given step
    Given I have a given step

Scenario: I have a scenario with given and when steps
    Given I have a given step
    When I have a when step

Scenario: I have a scenario with given and when and then steps
    Given I have a given step
    When I have a when step
    Then I have given and when steps