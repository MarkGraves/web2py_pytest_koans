from pytest_bdd import given
import pytest
import inspect
# pytest-bdd givens are fixtures per pytest_bdd/steps.py @ line 139

@pytest.fixture()
def sample_given_fixture():
    return 'given'

def test_given_is_a_pytest_fixture(sample_given_fixture):
    assert(inspect.isclass(sample_given_fixture), True)
    print sample_given_fixture.__class__.__name__
    print type(sample_given_fixture).__name__
    assert(sample_given_fixture == 'given')

def test_given_can_accept_fixture_arguments():
    given('I have a given statement', fixture='sample_given_fixture')
    pass
