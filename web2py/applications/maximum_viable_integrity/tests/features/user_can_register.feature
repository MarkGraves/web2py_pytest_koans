Feature: Registration

Scenario: Unregistered users should be able to register
    Given: I am not logged in
    When: I go to the registration page
    When: I input my registration data
    Then: The application should register my data